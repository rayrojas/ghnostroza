	$(document).ready(function(){
			$(".theme-changer").hide();
		$(".open-close").click(function(){
			$(".theme-changer").animate({width:'toggle'}, 300);
			});
	});

//Dropdown menu script
$(document).ready(function(){ 
	$("#menu ul.child").removeClass("child");
	$("#menu li").has("ul").hover(function(){
		$(this).addClass("active").children("ul").stop(true,true).fadeIn('fast');
	}, function() {
		$(this).removeClass("active").children("ul").fadeOut(100);
	});
});

//Img hover script

$(document).ready(function() {
	$('<span class="overlayered"></span>').insertAfter('.img-holder a img,.img-holder3 a img,.img-holder4 a img,.img-holder3 a img,.img-holder5 a img,.img-holder6 a img,.img-holder3 iframe');
	$(".overlayered").hover(function() {
		$(this).stop(true, true).animate({
			opacity: '0.9'
		}, 150);
	}, function() {
		$(this).stop(true, true).animate({
			opacity: '0'
		}, 500);
	});	
});

// Rubber search field

$(document).ready(function(){
	$('.search').focus(function() {
		$(this).stop().animate({
			"width": "200px"
		}, 300);
	}).blur(function() {
		$(this).animate({
			"width": "126px"
		}, 700);
	
	});
});

// Background color animation

$(document).ready(function() {
	$('.search-btn,input#send').mouseover(function() {
		$(this).stop().animate({
			"background-color": "#f0842d"
		}, 100);
	}).mouseout(function() {
		$(this).animate({
			"background-color": "#4c4c4c"
		}, 700);
	});
});

//Blocks alignment along height

function setEqualHeight(columns) {
	var tallestcolumn = 0;
	columns.each(function() {
		currentHeight = $(this).height();
		if (currentHeight > tallestcolumn) {
			tallestcolumn = currentHeight;
		}
	});
	columns.height(tallestcolumn);
}
$(document).ready(function() {
	setEqualHeight($(".wrap-960 > div.col"));
});

//Expandable footer script

jQuery(function($) {
	var container = $(".expandable");
	$("#open-close").click(function(event) {
		event.preventDefault();
		if (container.is(":visible")) {
			container.slideUp(300);
			$(this).removeClass("closed");
		} else {
			container.slideDown(200);
			$(this).addClass("closed");
		}
	});
});

//Expandable blog topbar script

jQuery(function($) {
	var container = $(".topbar");
	$(".switch").click(function(event) {
		event.preventDefault();
		if (container.is(":visible")) {
			container.slideUp(300);
			$(this).removeClass("active");
		} else {
			container.slideDown(200);
			$(this).addClass("active");
		}
	});
});

// Background color animation

$(document).ready(function() {
	$('#ca-container').contentcarousel();
});
