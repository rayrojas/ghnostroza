$(document).ready(function() {
	$('.fade-in').append('<span class="hover"></span>').each(function () {
	  var $span = $('> span.hover', this).css('opacity', 0);
	  $(this).hover(function () {
	    $span.stop().fadeTo(200, 1);
	  }, function () {
	    $span.stop().fadeTo(700, 0);
	  });
	});
});

