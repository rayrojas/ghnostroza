//Cufon settings
Cufon.replace("h1, #menu", {
	fontFamily: 'Zag Bold',
	hover: true
});

Cufon.replace(".r", {
	fontFamily: 'Zag Bold',
	hover: true
});

Cufon.replace("h3, h2, .numbs, .post-date span, #pagination", {
	fontFamily: 'Zag Bold',
	textShadow: '#fff 0 1px'
});
Cufon.replace(".box h3", {
	fontFamily: 'Zag Bold'
});
Cufon.replace(".testimonials", {
	fontFamily: 'Fontin Sans Rg'
});
SyntaxHighlighter.all();
