<?php
	class Alumno extends CI_Controller {
		public function index($amount = 0) {
			$this->load->library('aplicacion');
			$this->paralels = $this->load->database('paralels', TRUE);
			$this->moodle = $this->load->database('moodle', TRUE);
			if ( $amount != 0 ) {
				$bk_data_moodle = array(
										'alumnos' => $this->moodle->query('SELECT * FROM mdl_user LIMIT '.$amount )
										);
			} else {
				$bk_data_moodle = array(
										'alumnos' => $this->moodle->query('SELECT * FROM mdl_user')
										);				
			}
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			$data['data_moodle'] = $bk_data_moodle;
			$this->load->view('vhead', $data);
			$this->load->view('valumno', $data);
			$this->load->view('vfooter', $data);
		}
		public function form_alumno(){
			$this->load->library('aplicacion');
			$this->paralels = $this->load->database('paralels', TRUE);
			$this->moodle = $this->load->database('moodle', TRUE);
			$bk_aplicacion = $this->aplicacion->get_aplicacion();
			if ( isset($_FILES["file_csv"])) {
				move_uploaded_file($_FILES["file_csv"]["tmp_name"], $bk_aplicacion['media']."/csv/alumno/".$_FILES["file_csv"]["name"]);
				header('Location: '. $bk_aplicacion['site'].'?alumno/form_alumno/');
			}
			$filesCat = scandir($bk_aplicacion['media'].'/csv/alumno/');
			$data['data_csv']['list_csv'] = $filesCat;
			$bk_forms = array(
							'alumno' => true
							);
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			$data['forms'] = $bk_forms;
			$this->load->view('vhead', $data);
			$this->load->view('valumno', $data);
			$this->load->view('vfooter', $data);
		}
		public function data($hoja = "none", $amount = 0){
			$this->load->library('aplicacion');
			$bk_aplicacion = $this->aplicacion->get_aplicacion();
			if ( $hoja != "none" ) {
				$this->load->library('getcsv');
				$data = array('username', 'password', 'firstname', 'lastname');
				$csv_array = $this->getcsv->get_csv_assoc_array($bk_aplicacion['media']."/csv/alumno/".$hoja, $data, $amount);
				$bk_data_csv = array(
									'alumnos' => $csv_array
									);
				$data['data_csv'] = $bk_data_csv;
				$data['aplicacion'] = $this->aplicacion->get_aplicacion();
				$this->load->view('vhead', $data);
				$this->load->view('valumno', $data);
				$this->load->view('vfooter', $data);
			} else {
				header('Location: '. $bk_aplicacion['site'].'?alumno/form_alumno/');
			}
		}
		
		public function subir($hoja = "none"){
			$this->load->library('aplicacion');
			$bk_aplicacion = $this->aplicacion->get_aplicacion();
			if ( $hoja != "none" ) {
				$this->load->library('getcsv');
				$data = array('username', 'password', 'firstname', 'lastname');
				$csv_array = $this->getcsv->get_csv_assoc_array($bk_aplicacion['media']."/csv/alumno/".$hoja, $data);
				$bk_data_csv = array(
									'alumnos' => $csv_array
									);
				$data['data_csv'] = $bk_data_csv;
				$data['aplicacion'] = $this->aplicacion->get_aplicacion();
				$this->load->view('vhead', $data);
				$this->load->view('valumno', $data);
				$this->load->view('vfooter', $data);
			} else {
				header('Location: '. $bk_aplicacion['site'].'?alumno/form_alumno/');
			}
		}
	}
?>
