<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class Usuario extends CI_Controller {
		var $_id = 0;
		var $_nick = "";
		var $_clave = "";
		var $_nivel = "";
		var $_auth = FALSE;

		function __construct($auto = TRUE) {

			parent::__construct();
			$this->load->library('session');
			if($auto)
			{
				if($this->login($this->session->userdata('nick'), $this->session->userdata('clave')))
				{
					$this->_id = $this->session->userdata('id');
					$this->_nick = $this->session->userdata('nick');
					$this->_clave = $this->session->userdata('clave');
					$this->_nivel = $this->session->userdata('nivel');				
				}
			}
		}

		function login( $nick, $clave ) {
			return false;
		}


		function iniciosesion($idioma=null)
		   {
		         
		      //   $this->config->set_item('language', 'spanish');      //   Setear dinámicamente el idioma que deseamos que ejecute nuestra aplicación
		      if(!isset($_POST['maillogin'])){   //   Si no recibimos ningún valor proveniente del formulario, significa que el usuario recién ingresa.   
		         $this->load->view('login');      //   Por lo tanto le presentamos la pantalla del formulario de ingreso.
		      }
		      else{                        //   Si el usuario ya pasó por la pantalla inicial y presionó el botón "Ingresar"
		         $this->form_validation->set_rules('maillogin','e-mail','required|valid_email');      //   Configuramos las validaciones ayudandonos con la librería form_validation del Framework Codeigniter
		         $this->form_validation->set_rules('passwordlogin','password','required');
		         if(($this->form_validation->run()==FALSE)){            //   Verificamos si el usuario superó la validación
		            $this->load->view('login');                     //   En caso que no, volvemos a presentar la pantalla de login
		         }
		         else{                                       //   Si ambos campos fueron correctamente rellanados por el usuario,
		            $this->load->model('usuarios_model');
		            $ExisteUsuarioyPassoword=$this->usuarios_model->ValidarUsuario($_POST['maillogin'],$_POST['passwordlogin']);   //   comprobamos que el usuario exista en la base de datos y la password ingresada sea correcta
		            if($ExisteUsuarioyPassoword){   // La variable $ExisteUsuarioyPassoword recibe valor TRUE si el usuario existe y FALSE en caso que no. Este valor lo determina el modelo.
		               echo "Validacion Ok<br><br><a href=''>Volver</a>";   //   Si el usuario ingresó datos de acceso válido, imprimos un mensaje de validación exitosa en pantalla
		            }
		            else{   //   Si no logró validar
		               $data['error']="E-mail o password incorrecta, por favor vuelva a intentar";
		               $this->load->view('login',$data);   //   Lo regresamos a la pantalla de login y pasamos como parámetro el mensaje de error a presentar en pantalla
		            }
		         }
		      }
		   }

		function getId(){return $this->_id;}
		function getNick(){return $this->_nick;}
		function getClave(){return $this->_clave;}
		function getNivel(){return $this->_nivel;}	


	}
?>