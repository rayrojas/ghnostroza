<?php
	class Admin extends CI_Controller {
		function __construct() {

			parent::__construct();
			$this->load->library('aplicacion');
			$this->load->library('forms');
			$this->load->library('session');
			$this->load->database();

		}
		public function index($page = 'lectura' ) {
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			if ( $StrinClass == '' )
				header( 'Location: '.$data['aplicacion']['site'] );
		}

		function show_404($page = ''){ // error page logic
			header("HTTP/1.1 404 Not Found");
			$heading = "404 Page Not Found";
			$message = "The page you requested was not found ";
			$CI =& get_instance();
			$CI->load->view('/*Name of you custom 404 error page.*/');
		}

		public function sesion($action = ''){
			if ( $action == '' ){

			} elseif ( $action == 'salir' ) {
				# code...
			}

		}

		public function registro($StrinClass= '', $page = 'lista', $codigo = 0) {
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			if ( $StrinClass == '' ) {
				$this->load->view('admin/baseHead', $data);
				$this->load->view('admin/baseFooter', $data);				
			} else {
				$data['aplicacion']['static-js'] = '<script src="'.$data['aplicacion']['static'].'/admin/js/admin.site.utils.js"></script>';
				$this->load->model($StrinClass);
				if ( $page == 'lista' ) {
					$data['data'] = array(
											'section' => 'lista',
											'titulo' => 'Lista de '.ucfirst($StrinClass),
											'object-self' => $this->$StrinClass,
											'list_objects' => $this->$StrinClass->last('array')
										);
					$this->load->view('admin/baseHead', $data);
					$this->load->view('admin/servicios-list', $data);
					$this->load->view('admin/baseFooter', $data);
				} else if ( $page == 'add' ) {
					$ClassSTD = new $this->$StrinClass();
					/* Prepare Validation */
					$this->load->helper(array('form', 'url'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules($this->$StrinClass->model());

					/* --- */
					if ($_POST) {
						$data['data'] = array(
											'section' => 'registro',
											'titulo' => 'Datos de la '.ucfirst($StrinClass),
											'form' => $this->forms->models($ClassSTD),
											'object-self' => $this->$StrinClass,
											'message' => array()
										);
						if ( $this->form_validation->run() == TRUE ) {
							$ClassSTD->data($this->input->post());
							$ClassSTD->save();
							$data['data']['message'] = array('success' => 'Se guardaron los datos.');
							$data['data']['form'] = $this->forms->models($ClassSTD);
						} else {
							$data['data']['message'] = array('error' => 'No se guardaron los datos.');
							$data['data']['form'] = $this->forms->models($ClassSTD);
						}
					} else {
						$data['data'] = array(
											'section' => 'registro',
											'titulo' => 'Datos de la '.ucfirst($StrinClass),
											'form' => $this->forms->models($ClassSTD),
											'object-self' => $this->$StrinClass,
											'message' => array()
										);					
					}
					$this->load->view('admin/baseHead', $data);
					$this->load->view('admin/servicios-list', $data);
					$this->load->view('admin/baseFooter', $data);
				} else if ( $page == 'editar' && $codigo > 0 ) {
					/* JS - CSS */
					//$data['aplicacion']['static-js'] = '<script src="'.$data['aplicacion']['static'].'/admin/js/admin.site.register.js"></script>';
					/* --- */
					$ClassSTD = new $this->$StrinClass();
					/* Prepare Validation */
					$this->load->helper(array('form', 'url'));
					$this->load->library('form_validation');
					$this->form_validation->set_rules($this->$StrinClass->model());

					/* --- */
					if ($_POST) {
						$posted_object = $this->input->post();
						$hold_object = $ClassSTD->get($codigo);

						foreach ( $this->$StrinClass->model() as $field ) {
							if ( $field['type'] == 'input-file-image' ) {
								if ( $_FILES[$field['field']]['error'] == 4 ) {
									$this->form_validation->set_rules($field['field'], $field['verbose_name']);
									$_POST[$field['field']] = $hold_object->$field['field'];
								}
							}
						}
						$data['data'] = array(
											'section' => 'editar',
											'titulo' => 'Datos de '.ucfirst($StrinClass),
											'form' => $this->forms->models($ClassSTD),
											'object-self' => $this->$StrinClass,
											'object-change' => $codigo,
											'message' => array()
										);
						if ( $this->form_validation->run() == TRUE ) {
							$ClassSTD->data($this->input->post());
							$ClassSTD->update($codigo);
							$data['data']['message'] = array('success' => 'Se guardaron los datos.');
							$data['data']['form'] = $this->forms->models($ClassSTD->get($codigo));
						} else {
							$data['data']['message'] = array('error' => 'No se guardaron los datos.');
							$data['data']['form'] = $this->forms->models($ClassSTD);
						}
					} else {
						$data['data'] = array(
											'section' => 'editar',
											'titulo' => 'Datos de la '.ucfirst($StrinClass),
											'form' => $this->forms->models($ClassSTD->get($codigo)),
											'object-self' => $this->$StrinClass,
											'object-change' => $codigo,
											'message' => array()
										);					
					}
					$this->load->view('admin/baseHead', $data);
					$this->load->view('admin/servicios-list', $data);
					$this->load->view('admin/baseFooter', $data);
				}
			}
		}

		function upload($str, $choices) {
			$this->load->library('upload');
			$choices = explode(",", $choices);
			/* Imagen configuracion */
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			$config['upload_path'] = $data['aplicacion']['media_root'].$choices[1];
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '2000';
			$config['max_width']  = '2000';
			$config['max_height']  = '2000';
			
			$this->upload->initialize($config);
			/* Imagen configuracion */					
			if (isset($_FILES[$choices[0]]) && !empty($_FILES[$choices[0]]['name'])) {
				if ($this->upload->do_upload($choices[0])) {
					$upload_data    = $this->upload->data();
					$_POST[$choices[0]] = $choices[1].$upload_data['file_name'];
					/*$config['image_library'] = 'gd2';
					$config['source_image'] = $upload_data['full_path'];
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 100;
					$config['height'] = 75;
					$config['new_image'] = $data['aplicacion']['media_root'].$choices[2].$upload_data['file_name'];
					$this->load->library('image_lib', $config);
					if ( ! $this->image_lib->resize() ) {
						echo $this->image_lib->display_errors();
					}*/
					return true;
				} else {
					// possibly do some clean up ... then throw an error
					$this->form_validation->set_message('handle_upload', $this->upload->display_errors());
					return false;
				}
			}
			else {
				// throw an error because nothing was uploaded
				$this->form_validation->set_message('handle_upload', "You must upload an image!");
				return false;
			}
		}
	}
?>
