<?php
	class Front extends CI_Controller {
		public function index() {
			$this->load->library('aplicacion');
			$this->load->database();
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			/*slide*/
			$bk_query_result = $this->db->get('diapositiva');
			$data['db']['slide'] = $bk_query_result;
			/*endslide*/
			/*Recursos Humanos*/
			$data['db']['recursoshumanos'] = $this->db->get('recursoshumanos');
			/*endslide*/
			/*Productos Generales*/
			$data['db']['productosgenerales'] = $this->db->get('productosgenerales');
			/*endslide*/						
			$data['db']['servicios'] = $this->db->get('servicios');
			$data['db']['menu'] = $this->db->query("SELECT * FROM menu order by posicion");
			$this->load->view('baseHead', $data);
			$this->load->view('slide', $data);
			$this->load->view('mainmenu', $data);
			$this->load->view('index', $data);
			$this->load->view('baseFooter', $data);
		}
		public function servicios() {
			$this->load->library('aplicacion');
			$this->load->database();
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			/*slide*/
			$bk_query_result = $this->db->get('diapositiva');
			$data['db']['slide'] = $bk_query_result;
			/*endslide*/
			/*Recursos Humanos*/
			$data['db']['recursoshumanos'] = $this->db->get('recursoshumanos');
			/*endslide*/
			/*Productos Generales*/
			$data['db']['menu'] = $this->db->query("SELECT * FROM menu order by posicion");
			$data['db']['servicios'] = $this->db->get('servicios');
			/*endslide*/						
			$this->load->view('baseHead', $data);
			$this->load->view('mainmenu', $data);
			$this->load->view('servicios', $data);
			$this->load->view('baseFooter', $data);
		}
		public function contacto() {
			$this->load->library('aplicacion');
			$this->load->database();
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			/*slide*/
			$bk_query_result = $this->db->get('diapositiva');
			$data['db']['slide'] = $bk_query_result;
			/*endslide*/
			/*Recursos Humanos*/
			$data['db']['recursoshumanos'] = $this->db->get('recursoshumanos');
			/*endslide*/
			/*Productos Generales*/
			$data['db']['menu'] = $this->db->query("SELECT * FROM menu order by posicion");
			$data['db']['servicios'] = $this->db->get('servicios');
			/*endslide*/						
			$this->load->view('baseHead', $data);
			$this->load->view('mainmenu', $data);
			$this->load->view('contacto', $data);
			$this->load->view('baseFooter', $data);
		}
		public function trabaja_con_nosotros() {
			$this->load->library('aplicacion');
			$this->load->database();
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			/*slide*/
			$bk_query_result = $this->db->get('diapositiva');
			$data['db']['slide'] = $bk_query_result;
			/*endslide*/
			/*Recursos Humanos*/
			$data['db']['recursoshumanos'] = $this->db->get('recursoshumanos');
			/*endslide*/
			/*Productos Generales*/
			$data['db']['menu'] = $this->db->query("SELECT * FROM menu order by posicion");
			$data['db']['servicios'] = $this->db->get('servicios');
			/*endslide*/						
			$this->load->view('baseHead', $data);
			$this->load->view('mainmenu', $data);
			$this->load->view('trabaja_con_nosotros', $data);
			$this->load->view('baseFooter', $data);			
		}
	}
?>
