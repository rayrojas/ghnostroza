<?php
	class Curso extends CI_Controller {
		public function index() {
			$this->load->library('aplicacion');
			$this->paralels = $this->load->database('paralels', TRUE);
			$this->moodle = $this->load->database('moodle', TRUE);
			$bk_data_moodle = array(
									'cursos' => $this->moodle->get('mdl_course'),
									'categorias' => $this->moodle->get('mdl_course_categories'),
									'bloques' => $this->moodle->get('mdl_block_instance'),
									//'secciones' = $this->moodle->get('mdl_course_sections')
									);
			//print_r($bk_data_moodle['alumnos']->result());
			
			$data['aplicacion'] = $this->aplicacion->get_aplicacion();
			$data['data_moodle'] = $bk_data_moodle;
			$this->load->view('vcurso', $data);
		}
		
		
	}
?>
