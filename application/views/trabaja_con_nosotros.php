﻿
            <div id="page-descrp-wrap">
                <div id="page-descrp-top">
                    <div class="page-title-bg"></div>

                    <div id="page-descrp">
                        <h1>Trabaja con Nostros</h1>

                        <p>Deseamos unir fuerzas, envianos una solicitud.</p>

                        <div id="social-wrap">
                            <span class="soc-text">Visitenos en</span>

                            <div id="soc-icns">
                                <ul class="social-1">
                                    <li><a href="#" class="social6 fade-in"></a></li>

                                    <li><a href="#" class="social7 fade-in"></a></li>

                                    <li><a href="#" class="social8 fade-in"></a></li>

                                    <li><a href="#" class="social9 fade-in"></a></li>

                                    <li><a href="#" class="social10 fade-in"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="page-descrp-bottom"></div>

            </div><span class="addition1"><a href="index.html">Grupo Hinostroza</a><img class="separator" alt="" src="images/list-menu.gif" /><a href="#">Contactenos</a></span>
            
            <div class="clear"></div>

            <div class="dashed"></div>
            
            <!-- END HEADER -->
            <!-- START ABOUT -->

            <div class="block">
                <div class="wrap-620">
                    
                    <h2 class="selection">Formulario de Solicitud</h2>

                    <p class="space2">Grupo Hinostroza actualiza sus procesos acorde a la tecnologia, en esta ocación le presentamos nuestro formulario Contacto. El cuál será nuestro medio de comunicación entre Nuestros Clientes.</p>

	                <h3>Solicito:</h3>

	                <div class="space3 contact-form">
    	                <form id="contact" method="post" action="">
                            <div>
                                <input id="id_solicito" type="text" name="solicito" value="Solicito" onfocus="if (this.value=='Solicito') this.value='';" onblur="if (this.value==''){this.value='Solicito'}" />
                            </div>
                            <div>
                                <h3>Mis Datos son:</h3>
                                <input id="id_name" type="text" name="name" value="Nombres" onfocus="if (this.value=='Nombres') this.value='';" onblur="if (this.value==''){this.value='Nombres'}" />
                            </div>
                            <div>
                                <input id="id_email" type="text" name="email" value="Correo Electrónico" onfocus="if (this.value=='Correo Eletrónico') this.value='';" onblur="if (this.value==''){this.value='Correo Eletrónico'}" />
                            </div>
                            <div>
                                <h3>Representante de (Opcional):</h3>
                                <input id="id_representante" type="text" name="name" value="Empresa (Opcional)" onfocus="if (this.value=='Empresa (Opcional)') this.value='';" onblur="if (this.value==''){this.value='Empresa (Opcional)'}" />
                            </div>                            
                            <div>
                                <h3>Adjunto el siguiente archivo (Opcional):</h3>
                                <input id="id_file" type="file" name="adjunto" />
                            </div>
                	        <div>
                                <h3>Detalles:</h3>
                    	        <textarea id="id_message" name="message" cols="" rows=""></textarea>
                        	</div>
   		                    <div>
        	                    <input id="send" name="submit" type="submit" value="Enviar" /><input type="hidden" name="comment_post_ID" value="16" />
            	            </div>
                	    </form>
                	</div>
                </div>
                
                <!-- START GO TO SERVICES -->

                <div class="short-info">
                    <h2><img src="<?php echo $aplicacion['static']; ?>/images/arr-to-left.png" alt="" /><span class="selection">Buscamos personas</span></h2>
                    <p class="space">Grupo Hinostroza cuenta con Personas decididas a emprender el Reto de crecer con nosotros.</p>
                    <h3>Valores que prácticamos</h3>
                    <p class="testimonials">Responsabilidad, el trabajo que nos encomiendan es lo primero.</p>
                    <p class="testimonials">Puntualidad, tu tiempo es lo más importante. Lo tenemos muy claro.</p>
                </div>

                <div class="clear"></div>

                <div class="divider4"></div>
            </div>
            
            <!-- END GO TO SERVICES -->
            <!-- END ABOUT -->
            <!-- START LAST TWEET -->
            <!-- END LAST TWEET -->
            <!-- START FOOTER -->