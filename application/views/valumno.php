    <div class="container-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
						<li class="nav-header">Alumnos</li>
						<li class="active"><a href="#">Acciones Masivas</a></li>
						<li><a href="#">Buscar Alumnos</a></li>
						<li><a href="#">Matricular Alumno</a></li>
						<li><a href="#">Expulsar Alumno</a></li>
						<li class="nav-header">Profesores</li>
						<li><a href="#">Acciones Masivas</a></li>
						<li><a href="#">Buscar Profesores</a></li>
						<li><a href="#">Ingresar Profesor</a></li>
						<li><a href="#">Habilitar/Desahilitar</a></li>
						<li><a href="#">Asignar Curso</a></li>
						<li class="nav-header">Cursos</li>
						<li><a href="#">Lista de Cursos</a></li>
						<li><a href="#">Crear curso</a></li>
						<li><a href="#">Buscar Curso</a></li>
					</ul>
				</div><!--/.well -->
			</div><!--/span-->
			<div class="span9">
				<div class="row-fluid">
					<div class="span1 action-btn round-all">
						<a href="<?php echo $aplicacion['root_site'] ?>?alumno/">
							<div>
								<i class="icon-list"></i>
							</div>
							<div>
								<strong>Lista</strong>
							</div>
						</a>
					</div>
					<div class="span1 action-btn round-all">
						<a href="#">
							<div>
								<i class="icon-search"></i>
							</div>
							<div>
								<strong>Buscar</strong>
							</div>
						</a>
					</div>
					<div class="span1 action-btn round-all">
						<a href="<?php echo $aplicacion['root_site'] ?>?alumno/form_alumno/">
							<div>
								<i class="icon-upload"></i>
							</div>
							<div>
								<strong>CSV</strong>
							</div>
						</a>
					</div>
				</div>
			<?php if ( isset($data_moodle['alumnos']) ): ?>
			<div class="row-fluid">
				<span class="span12">
					<div class="row-fluid" style="margin-bottom:10px;">
						<h4 class="pull-left">Lista de Alumnos</h4>
						<div class="btn-group pull-right">
							<a class="btn btn-danger dropdown-toggle" href="#" data-toggle="dropdown">
								Mostrar <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/index/10/">10</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/index/20/">20</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/index/50/">50</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/index/100/">100</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/index/">Todo</a></li>
							</ul>
						</div>
					</div>					
					<table class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th>Código</th>
								<th>Alumno</th>
								<th>Código</th>
								<th>Código</th>
								<th>Código</th>
								<th>Código</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($data_moodle['alumnos']->result() as $row): ?>
							<tr>
								<td><?php echo $row->username ?></td>
								<td><?php echo $row->firstname ?> <?php echo $row->lastname ?> </td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>			
				</span>
			</div>
			<?php endif; ?>
			<?php if ( isset($forms['alumno']) ): ?>
			<div class="row-fluid">
				<span class="span12">
					<h4>Formulario Alumnos</h4>
					<form class="form-horizontal well" action="<?php echo $aplicacion['root_site'] ?>?alumno/form_alumno/"  enctype="multipart/form-data"  method="POST">
						<fieldset>
							<div class="control-group">
								<label class="control-label" for="fileInput">Archivo CSV</label>
								<div class="controls">
									<input id="fileInput" class="input-file" type="file" name="file_csv">
									<p class="help-block">Seleccione un archivo csv de su computadora.</p>
								</div>
							</div>
							<div class="form-actions">
								<button class="btn btn-primary" type="submit">Subir Archivo</button>
								<button class="btn" type="reset">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</span>
			</div>
			<?php endif; ?>
			<?php if ( isset($data_csv['list_csv']) ): ?>
			<div class="row-fluid">
				<span class="span12">
					<h4>Archivos CSV de Alumnos</h4>
					<table class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th>Archivo</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $data_csv['list_csv'] as $row ): ?>
							<tr>
								<td><a href="/php/www/paralels/?alumno/data/<?php echo $row ?>/10/"><?php echo $row ?></a></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</span>
			</div>
			<?php endif; ?>
			<?php if ( isset($data_csv['alumnos']) ): ?>
			<div class="row-fluid">
				<span class="span12">
					<div class="row-fluid">
						<h4 class="pull-left">Lista de Alumnos</h4>
						<div class="btn-group pull-right">
							<a class="btn btn-danger dropdown-toggle" href="#" data-toggle="dropdown">
								Mostrar <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/data/alumnado.csv/10/">10</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/data/alumnado.csv/20/">20</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/data/alumnado.csv/50/">50</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/data/alumnado.csv/100/">100</a></li>
								<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/data/alumnado.csv/">Todo</a></li>
							</ul>
						</div>
					</div>
					<table class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th>Código</th>
								<th>Contraseña</th>
								<th>Nombre</th>
								<th>Apellidos</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $data_csv['alumnos'] as $row ): ?>
							<tr>
								<td><?php echo $row['username'] ?></td>
								<td><?php echo $row['password'] ?></td>
								<td><?php echo $row['firstname'] ?></td>
								<td><?php echo $row['lastname'] ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div>
						<p>
							<a href="<?php echo $aplicacion['root_site'] ?>?alumno/subir/alumnado.csv/" class="btn btn-primary">Subir a Base de Datos</a>
						</p>
					</div>
				</span>
			</div>
			<?php endif; ?>			
        </div>
      </div>

      <hr>

      <footer>
        <p>© Company 2012</p>
      </footer>

    </div><!--/.fluid-container-->
