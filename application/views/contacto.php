﻿            <div id="page-descrp-wrap">
                <div id="page-descrp-top">
                    <div class="page-title-bg"></div>

                    <div id="page-descrp">
                        <h1>Contactenos</h1>

                        <p>Consulte sus dudas, envie comentarios.</p>

                        <div id="social-wrap">
                            <span class="soc-text">Visitenos en</span>

                            <div id="soc-icns">
                                <ul class="social-1">
                                    <li><a href="#" class="social6 fade-in"></a></li>

                                    <li><a href="#" class="social7 fade-in"></a></li>

                                    <li><a href="#" class="social8 fade-in"></a></li>

                                    <li><a href="#" class="social9 fade-in"></a></li>

                                    <li><a href="#" class="social10 fade-in"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="page-descrp-bottom"></div>

            </div><span class="addition1"><a href="index.html">Grupo Hinostroza</a><img class="separator" alt="" src="images/list-menu.gif" /><a href="#">Contactenos</a></span>
            
            <div class="clear"></div>

            <div class="dashed"></div>
            
            <!-- END HEADER -->
            <!-- START ABOUT -->

            <div class="block">
                <div class="wrap-620">
                    
                    <h2 class="selection">Esperamos sus mensajes</h2>

                    <p class="space2">Grupo Hinostroza actualiza sus procesos acorde a la tecnologia, en esta ocación le presentamos nuestro formulario Contacto. El cuál será nuestro medio de comunicación entre Nuestros Clientes.</p>

	                <h3>Mensaje:</h3>

	                <div class="space3 contact-form">
    	                <form id="contact" method="post" action="">
        	                <div>
            	                <input id="name" type="text" name="name" value="Nombres" onfocus="if (this.value=='Nombres') this.value='';" onblur="if (this.value==''){this.value='Nombres'}" />
                	        </div>
                    	    <div>
                        	    <input id="email" type="text" name="email" value="Correo Electrónico" onfocus="if (this.value=='Correo Eletrónico') this.value='';" onblur="if (this.value==''){this.value='Correo Eletrónico'}" />
	                        </div>
                	        <div>
                    	        <textarea id="message" name="message" cols="" rows="">
</textarea>
                        	</div>
   		                    <div>
        	                    <input id="send" name="submit" type="submit" value="Enviar" /><input type="hidden" name="comment_post_ID" value="16" />
            	            </div>
                	    </form>
                	</div>
                </div>
                
                <!-- START GO TO SERVICES -->

                <div class="short-info">
                    <h2><img src="<?php echo $aplicacion['static']; ?>/images/arr-to-left.png" alt="" /><span class="selection">Calidad en Nuestros Productos</span></h2>
                    <p class="space">GoodYear proporciona las mejores llantes del mundo. Lo cual nos permite vender calidad.</p>
                    <h3>Testimonios</h3>
                    <p class="testimonials">Se reventó una llanta en el tramo de Lomo Largo, necesitaba ayuda inmediata. Una unidad móvil DEC, fue mi solución.</p>
                    <p class="author">Carlos Carhuaz Q., Conductor minero.</p>
                </div>

                <div class="clear"></div>

                <div class="divider4"></div>
            </div>
            
            <!-- END GO TO SERVICES -->
            <!-- END ABOUT -->
            <!-- START LAST TWEET -->
            <!-- END LAST TWEET -->
            <!-- START FOOTER -->