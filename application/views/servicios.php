﻿			<div id="page-descrp-wrap">
				<div id="page-descrp-top">
					<div class="page-title-bg"></div>
					<div id="page-descrp">
						<h1>Nuestros Servicios</h1>
						<p>Equipo Técnico Capacitado</p>
						<div id="social-wrap">
							<span class="soc-text">Visitanos en </span>
							<div id="soc-icns">
								<ul class="social-1">
									<li><a href="#" class="social6 fade-in"></a></li>
									<li><a href="#" class="social7 fade-in"></a></li>
									<li><a href="#" class="social8 fade-in"></a></li>
									<li><a href="#" class="social9 fade-in"></a></li>
									<li><a href="#" class="social10 fade-in"></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div id="page-descrp-bottom"></div>
			</div>
			<!-- END HEADER -->
			<!-- START POSTS -->
			<div class="block">
                <?php foreach ( $db['servicios']->result() as $itemservicios ) { ?>
				<div class="post" id="<?php echo $itemservicios->nombre ?>">
					<div class="img-holder6">
						<a><img src="<?php echo $aplicacion['media']; ?>/<?php echo $itemservicios->imagen ?>" alt="<?php echo $itemservicios->nombre ?>" /></a>
					</div>
					<div class="inline">
						<h2><a><?php echo $itemservicios->nombre ?></a></h2>
						<div class="addition space5">Publicado por <a class="selection" href="#">Casas Montiveros</a>
							<img class="small-icns" src="images/post-author-icon.png" alt="" />
							<img class="separator" src="images/list-menu.gif" alt="" />ver <a href="#">Perfil</a>
							<img class="small-icns" src="images/post-cat-icon.png" alt="" />
						</div>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur ...</p>
						<div class="space3">
							<a href="#" class="selection">Más</a>
						</div>
					</div>
				</div>
                <?php } ?>
