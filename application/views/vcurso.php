<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $aplicacion['titulo']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo $aplicacion['static']; ?>/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="fluid_files/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#"><?php echo $aplicacion['titulo']; ?></a>
          <div class="btn-group pull-right">
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="icon-user"></i> Usuario
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#">Perfil</a></li>
              <li class="divider"></li>
              <li><a href="#">Salir</a></li>
            </ul>
          </div>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">Inicio</a></li>
              <li><a href="#about">Alumnos</a></li>
              <li><a href="#about">Docentes</a></li>
              <li><a href="#contact">Cursos</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Alumnos</li>
              <li class="active"><a href="#">Acciones Masivas</a></li>
              <li><a href="#">Buscar Alumnos</a></li>
              <li><a href="#">Matricular Alumno</a></li>
              <li><a href="#">Expulsar Alumno</a></li>
              <li class="nav-header">Profesores</li>
              <li><a href="#">Acciones Masivas</a></li>
              <li><a href="#">Buscar Profesores</a></li>
              <li><a href="#">Ingresar Profesor</a></li>
              <li><a href="#">Habilitar/Desahilitar</a></li>
              <li><a href="#">Asignar Curso</a></li>
              <li class="nav-header">Cursos</li>
              <li><a href="#">Lista de Cursos</a></li>
              <li><a href="#">Crear curso</a></li>
              <li><a href="#">Buscar Curso</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="row-fluid">
			<div class="span1 action-btn round-all">
				<a href="#">
					<div>
						<i class="icon-list"></i>
					</div>
					<div>
						<strong>Lista</strong>
					</div>
				</a>
			</div>
			<div class="span1 action-btn round-all">
				<a href="#">
					<div>
						<i class="icon-search"></i>
					</div>
					<div>
						<strong>Buscar</strong>
					</div>
				</a>
			</div>
          </div>
			<?php if ( isset($data_moodle['cursos']) ): ?>
			<div class="row-fluid">
				<span class="span12">
					<h4>Cursos</h4>
					<table class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th>Código</th>
								<th>Categoria</th>
								<th>Curso</th>
								<th>Código</th>
								<th>Código</th>
								<th>Código</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $data_moodle['cursos']->result() as $row ): ?>
							<?php if ( $row->id != 1 ): ?>
							<tr>
								<td><?php echo $row->id ?></td>
								<td><?php echo $row->category ?></td>
								<td><?php echo $row->shortname ?> - <?php echo $row->fullname ?></td>
								<td></td>
							</tr>
							<?php endif; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</span>
			</div>
			<?php endif; ?>
			<?php if ( isset($data_moodle['categorias']) ): ?>
			<div class="row-fluid">
			<span class="span12">
				<h4>Categorias</h4>
				<table class="table table-striped table-bordered table-condensed">
					<thead>
						<tr>
							<th>Código</th>
							<th>Categoria</th>
							<th>Descipción</th>
							<th>Código</th>
							<th>Código</th>
							<th>Código</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ( $data_moodle['categorias']->result() as $row ): ?>
						<?php if ( $row->id != 1 ): ?>
						<tr>
							<td><?php echo $row->id ?></td>
							<td><?php echo $row->name ?></td>
							<td><?php echo $row->description ?></td>
						</tr>
						<?php endif; ?>
						<?php endforeach; ?>
					</tbody>
				</table>
			</span>
          </div>
          <?php endif; ?>
          <?php if ( isset($data_moodle['bloques']) ): ?>
          <div class="row-fluid">
			<span class="span12">
				<h4>Secciones</h4>
				<table class="table table-striped table-bordered table-condensed">
					<thead>
						<tr>
							<th>Código</th>
							<th>Curso</th>
							<th>Type</th>
							<th>Código</th>
							<th>Código</th>
							<th>Código</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ( $data_moodle['bloques']->result() as $row ): ?>
						<?php if ( $row->id != 1 ): ?>
						<tr>
							<td><?php echo $row->blockid ?></td>
							<td><?php echo $row->pageid ?></td>
							<td><?php echo $row->pagetype ?></td>
						</tr>
						<?php endif; ?>
						<?php endforeach; ?>
					</tbody>
				</table>
			</span>
          </div>
          <?php endif; ?>
			<?php if ( isset($forms['categoria']) ): ?>
				<div class="row-fluid">
					<span class="span12">
						<h4>Formulario Categorias</h4>
						<form class="form-horizontal well" action="/php/www/paralels/?curso/form_categorias/"  enctype="multipart/form-data"  method="POST">
							<fieldset>
								<div class="control-group">
									<label class="control-label" for="fileInput">Archivo CSV</label>
									<div class="controls">
										<input id="fileInput" class="input-file" type="file" name="file_csv">
										<p class="help-block">Seleccione un archivo csv de su computadora.</p>
									</div>
								</div>
								<div class="form-actions">
									<button class="btn btn-primary" type="submit">Subir Archivo</button>
									<button class="btn" type="reset">Cancelar</button>
								</div>								
							</fieldset>
						</form>
					</span>
				</div>
			<?php endif; ?>
			<?php if ( isset($data_csv['categoria']) ): ?>
				<div class="row-fluid">
					<span class="span12">
						<h4>Categorias</h4>
						<table class="table table-striped table-bordered table-condensed">
							<thead>
								<tr>
									<th>Código</th>
									<th>Categoria</th>
									<th>Descipción</th>
									<th>Código</th>
									<th>Código</th>
									<th>Código</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $data_csv['categoria'] as $row ): ?>
								<tr>
									<td><?php echo $row['username'] ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</span>
				</div>
			<?php endif; ?>
			<?php if ( isset($data_csv['list_csv']) ): ?>
				<div class="row-fluid">
					<span class="span12">
						<h4>Categorias</h4>
						<table class="table table-striped table-bordered table-condensed">
							<thead>
								<tr>
									<th>Archivo</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $data_csv['list_csv'] as $row ): ?>
								<tr>
									<td><a href="/php/www/paralels/?curso/form_categorias/<?php echo $row ?>"><?php echo $row ?></a></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</span>
				</div>
			<?php endif; ?>
        </div>
      </div>

      <hr>

      <footer>
        <p>© Company 2012</p>
      </footer>

    </div>
</body></html>
