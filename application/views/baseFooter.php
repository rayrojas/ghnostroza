            <div id="footer">
                    <div class="col">
                        <a href="index.html"><img alt="Grupo Hinostroza" src="<?php echo $aplicacion['static']; ?>/images/logox220.png"></a>
                        <div class="indent">
                            <p>Una empresa lider con mas de 38 años de experiencia en la distribución de llantas para todo de vehiculos.</p>
                            <div class="space2">
                                <a class="selection" href="#">contactenos</a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <h3>Ubicación</h3>
                        <div class="indent">
                            <div class="space">
                                <p>Av. Mareategui #333 - Huancayo / Teléfono: 345686</p>
                                <p>Av. Chilca #333 - Huancayo / Teléfono: 345686</p>
                            </div>
                            <div class="dashed"></div>
                            <div class="space">
                                Av. Mareategui #333 - Cerro de Pasco / Teléfono: 345686
                            </div>
                            <div class="dashed"></div>
                            <div class="space">
                                Av. Chilca #333 - Huanuco / Teléfono: 345686
                            </div>
                            <div class="dashed"></div>
                            <div class="space">
                                Av. La Marina cuadra 3 - Lima / Teléfono: 345686
                            </div>
                        </div>
                    </div>
                    <div class="col-last">
                        <iframe width="425" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.es/maps?source=s_q&amp;hl=es-419&amp;q=grupo+hinostroza+sac&amp;aq=0&amp;oq=Huancayo,+Junin,+Per%C3%BA+grupo&amp;sll=-9.189967,-75.015152&amp;sspn=23.567485,43.198242&amp;g=peru&amp;ie=UTF8&amp;hq=grupo+hinostroza+sac&amp;hnear=2+de+Mayo,+Chilca,+Huancayo,+Junin,+Per%C3%BA&amp;geocode=FWuoR_8dRJGE-w&amp;t=m&amp;ll=-12.063058,-75.215176&amp;spn=0.039571,0.037479&amp;output=embed"></iframe><br /><small><a href="https://maps.google.es/maps?source=embed&amp;hl=es-419&amp;q=grupo+hinostroza+sac&amp;aq=0&amp;oq=Huancayo,+Junin,+Per%C3%BA+grupo&amp;sll=-9.189967,-75.015152&amp;sspn=23.567485,43.198242&amp;g=peru&amp;ie=UTF8&amp;hq=grupo+hinostroza+sac&amp;hnear=2+de+Mayo,+Chilca,+Huancayo,+Junin,+Per%C3%BA&amp;geocode=FWuoR_8dRJGE-w&amp;t=m&amp;ll=-12.063058,-75.215176&amp;spn=0.039571,0.037479" style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>                        
                    </div>
                    <div class="clear"></div>
                    <!-- START EXPANDABLE FOOTER -->
                    <!-- END EXPANDABLE FOOTER -->
                </div>

				<div class="copyrights">
					<p>&copy; 2012 Todo el Contenido bajo los Terminos de propiedad intelectual de Grupo Hinostroza</p>
					<ul class="social">
						<li><a href="#" class="social1 fade-in"></a></li>
						<li><a href="#" class="social2 fade-in"></a></li>
						<li><a href="#" class="social3 fade-in"></a></li>
						<li><a href="#" class="social4 fade-in"></a></li>
						<li><a href="#" class="social5 fade-in"></a></li>
					</ul>
				</div>
			<!-- END FOOTER -->
			</div>
		</div><script type="text/javascript">
		//<![CDATA[
			Cufon.now(); 
		//]]>
		</script>
	</body>
</html>
