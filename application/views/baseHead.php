<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title><?php echo $aplicacion['titulo']; ?></title>
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/js/anything-slider/slider.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/js/jFlickr/jflickr_css/style.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/js/social/social.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/js/colorbox/colorbox.css" type="text/css" media="screen" />		
        <link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<!--[if IE 7]><link rel="stylesheet" href="style/ie7.css" type="text/css" /><![endif]-->
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jquery.effects.core.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/contact-form/contactform.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/anything-slider/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/anything-slider/jquery.anythingslider.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/fancybox.settings.js"></script>        
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/slider-settings.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/cufon/cufon-yui.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/cufon/generated-fonts.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/cufon-settings.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jPreloader/jquery.preloader.js"></script>
        <script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jPreloader-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/tweet/jquery.tweet.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jquery.tweet-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jFlickr/jflickr_js/jflickr_0.3_min.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jflickr-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/colorbox/jquery.colorbox-min.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/colorbox-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/masonry/jquery.masonry.min.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jquery.masonry-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/social/social.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/jQuery-tools/jquery.tools.min.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/scrollable-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/galleria/galleria-1.2.5.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/galleria-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/img-center/imgCenter.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/imgCenter-settings.js"></script>
		<script type="text/javascript" src="<?php echo $aplicacion['static']; ?>/js/overall-effects.js"></script>
		<link rel="shortcut icon" href="<?php echo $aplicacion['static']; ?>/images/logo-min.png" type="image/x-icon" />
	</head>
	<body>
	<div id="outer">
		<div id="page-wrapper">