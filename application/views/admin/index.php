				<div class="span9">
					<h2>
						<?php echo $data['titulo']; ?>
					</h2>
					<?php if (!empty($data['message'])): ?>
						<?php foreach($data['message'] as $key => $value) {
							echo '<div class="alert alert-'.$key.'">';
							echo $value;
							echo '</div>';
						} ?>
					<?php endif; ?>
					<?php if ( $data['section'] == 'lectura' ): ?>
					<?php echo form_open_multipart($aplicacion['site'].$data['action']); ?>
						<?php echo $data['form']; ?>
						<div class="control-group">
							<div class="controls">
								<button id="btn-editar" type="button" class="btn btn-success">Editar</button>
								<button id="btn-enviar" type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
					<?php endif; ?>
				</div>
			</div>
		</div>
