<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="ie6 ielt7 ielt8 ielt9"><![endif]--><!--[if IE 7 ]><html lang="en" class="ie7 ielt8 ielt9"><![endif]--><!--[if IE 8 ]><html lang="en" class="ie8 ielt9"><![endif]--><!--[if IE 9 ]><html lang="en" class="ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en"><!--<![endif]--> 
	<head>
		<meta charset="utf-8">
		<title>Administración - <?php echo $aplicacion['titulo']; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/admin/css/bootstrap.min.css"  type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/admin/css/bootstrap-responsive.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $aplicacion['static']; ?>/admin/css/site.css" type="text/css" media="screen" />		
		<?php
			if ( !empty($aplicacion['static-css']) )
				echo $aplicacion['static-css'];
		?>
		<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<body>
		<div class="container">
			<div class="navbar">
				<div class="navbar-inner">
					<div class="container">
						<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="<?php echo $aplicacion['site']; ?>?admin/">Admin <?php echo $aplicacion['titulo']; ?></a>
						<div class="nav-collapse">
							<ul class="nav">
								<li class="active">
									<a href="<?php echo $aplicacion['site']; ?>?admin/">Inicio</a>
								</li>
							</ul>
							<ul class="nav pull-right">
								<li>
									<a href="profile.htm">Lokcito</a>
								</li>
								<li>
									<a href="login.htm">Salir</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="span2">
					<div class="well" style="padding: 8px 0;">
						<ul class="nav nav-list">
							<li class="nav-header">
								<?php echo $aplicacion['titulo']; ?>
							</li>
							<li class="active">
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/"><i class="icon-white icon-home"></i> Inicio</a>
							</li>
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/menu/"><i class="icon-list"></i> Menu</a>
							</li>							
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/diapositiva/"><i class="icon-picture"></i> Diapositivas</a>
							</li>
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/recursoshumanos/"><i class="icon-th-large"></i> Recursos Humanos</a>
							</li>
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/productosgenerales/"><i class="icon-envelope"></i> Productos Generales</a>
							</li>
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/servicios/"><i class="icon-file"></i> Servicios</a>
							</li>
							<li>
								<a href="<?php echo $aplicacion['site']; ?>?admin/registro/valor/"><i class="icon-list-alt"></i> Valores</a>
							</li>
						</ul>
					</div>
				</div>
