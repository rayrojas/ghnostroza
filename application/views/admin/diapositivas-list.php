				<div class="span9">
					<h2>
						<?php echo $data['titulo'] ?>
					</h2>
					<?php if ( $data['section'] == 'lista' ): ?>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>
									Imagen
								</th>
								<th>
									Fecha de Subida
								</th>
								<th>
									Imagen Grande
								</th>
								<th>
									Imagen Pequeña
								</th>
								<th>
									Fondo
								</th>								
								<th>
									<a class="btn btn-mini btn-primary" href="<?php echo $aplicacion['site']; ?>?admin/diapositivas/registro/"><i class="icon-plus icon-white"></i></a>
									Acciones
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach( $data['diapositivas'] as $diapositiva ) { ?>
							<tr>
								<td>
									<?php echo $diapositiva->nombre ?>
								</td>
								<td>
									<?php echo $diapositiva->fecha ?>
								</td>
								<td>
									<img class="thumb-100" src="<?php echo $aplicacion['media'];?>/<?php echo $diapositiva->imagen_max ?>">
								</td>
								<td>
									<img class="thumb-100" src="<?php echo $aplicacion['media'];?>/<?php echo $diapositiva->imagen_min ?>">
								</td>
								<td>
									<img class="thumb-100" src="<?php echo $aplicacion['media'];?>/<?php echo $diapositiva->fondo ?>">
								</td>																
								<td>
									<a href="<?php echo $aplicacion['site']; ?>?admin/diapositivas/editar/<?php echo $diapositiva->id ?>/" class="view-link">Editar</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<?php elseif ( $data['section'] == 'registro' ): ?>
					<ul class="pager">
						<li class="next">
							<a href="<?php echo $aplicacion['site']; ?>?admin/diapositivas/">Lista de Diapositivas &rarr;</a>
						</li>
					</ul>					
					<?php echo form_open_multipart($aplicacion['site'].$data['action']); ?>
						<?php echo $data['form'] ?>
						<div class="control-group">
							<div class="controls">
								<button id="btn-enviar" type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
					<?php endif;?>
					<?php if ( $data['section'] == 'editar' ): ?>
					<ul class="pager">
						<li class="next">
							<a href="<?php echo $aplicacion['site']; ?>?admin/diapositivas/">Lista de Diapositivas &rarr;</a>
						</li>
					</ul>						
					<?php if (!empty($data['message'])): ?>
						<?php foreach($data['message'] as $key => $value) {
							echo '<div class="alert alert-'.$key.'">';
							echo $value;
							echo '</div>';
						} ?>
					<?php endif; ?>					
					<?php echo form_open_multipart($aplicacion['site'].$data['action']); ?>
						<?php echo $data['form']; ?>
						<div class="control-group">
							<div class="controls">
								<button id="btn-editar" type="button" class="btn btn-success">Editar</button>
								<button id="btn-enviar" type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
					<?php endif; ?>					
				</div>
			</div>
		</div>
