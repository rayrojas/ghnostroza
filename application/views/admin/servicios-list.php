				<div class="span10">
					<h2>
						<?php echo $data['titulo'] ?>
					</h2>
					<?php if ( $data['section'] == 'lista' ): ?>
					<table class="table table-bordered table-striped datatable">
						<thead>
							<tr>
								<?php
								foreach (get_object_vars($data['object-self']) as $key => $value) {
								 	print '<th>'.$key.'</th>';
								}
								?>
								<th>
									<a class="btn btn-mini btn-primary" href="<?php echo $aplicacion['site']; ?>?admin/registro/<?php echo strtolower(get_class($data['object-self'])); ?>/add/"><i class="icon-plus icon-white"></i></a>
									Acciones
								</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach( $data['list_objects'] as $item_object ) {
								print '<tr>';
								foreach ($data['object-self']->model() as $model_field) {
									if ( $model_field['type'] == 'input-file-image') {
										print '<td><img class="thumb-100" src="'.$aplicacion['media'].'/'.$item_object[$model_field['field']].'" /></td>';
									} else {
								 		print '<td>'.$item_object[$model_field['field']].'</td>';
								 	}
								 }
								print '<td><a href="'.$aplicacion['site'].'?admin/registro/'.strtolower(get_class($data['object-self'])).'/editar/'.$item_object['id'].'/">Editar</a></td>';
								print '</tr>';
							} ?>
						</tbody>
					</table>
					<?php elseif ( $data['section'] == 'registro' ): ?>
					<ul class="pager">
						<li class="next">
							<a href="<?php echo $aplicacion['site']; ?>?admin/registro/<?php echo strtolower(get_class($data['object-self'])); ?>/">Lista de <?php echo strtolower(get_class($data['object-self'])); ?>s &rarr;</a>
						</li>
					</ul>					
					<?php echo form_open_multipart($aplicacion['site'].'?admin/registro/'.strtolower(get_class($data['object-self']))."/add/"); ?>
						<?php echo $data['form'] ?>
						<div class="control-group">
							<div class="controls">
								<button id="btn-enviar" type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
					<?php endif;?>
					<?php if ( $data['section'] == 'editar' ): ?>
					<ul class="pager">
						<li class="next">
							<a href="<?php echo $aplicacion['site']; ?>?admin/registro/<?php echo strtolower(get_class($data['object-self'])); ?>/">Lista de <?php echo strtolower(get_class($data['object-self'])); ?>s &rarr;</a>
						</li>
					</ul>						
					<?php if (!empty($data['message'])): ?>
						<?php foreach($data['message'] as $key => $value) {
							echo '<div class="alert alert-'.$key.'">';
							echo $value;
							echo '</div>';
						} ?>
					<?php endif; ?>					
					<?php echo form_open_multipart($aplicacion['site'].'?admin/registro/'.strtolower(get_class($data['object-self'])).'/editar/'.$data['object-change']."/"); ?>
						<?php echo $data['form']; ?>
						<div class="control-group">
							<div class="controls">
								<button id="btn-editar" type="button" class="btn btn-success">Editar</button>
								<button id="btn-enviar" type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
					<?php endif; ?>					
				</div>
			</div>
		</div>
