            <!-- START CHECK IT! -->
            <div id="check">
                <div id="check-top"></div>
                <div class="transp">
                    <a href=""><img src="<?php echo $aplicacion['static']; ?>/images/goodyear_logo.png" alt="goodyear" title="GoodYear" /></a>
                    <a href="#"><img src="<?php echo $aplicacion['static']; ?>/images/texaco_logo.png" alt="texaco" title="Texaco" /></a>
                    <a href="#"><img src="<?php echo $aplicacion['static']; ?>/images/jmc_logo.png" alt="JMC" /></a>
                    <a href="#"><img src="<?php echo $aplicacion['static']; ?>/images/darwin_racing_logo.png" alt="Darwin Racing" /></a>
					<a id="mail-corporativo" href="http://www.hotmail.com/"><img src="<?php echo $aplicacion['static']; ?>/images/mail.png" alt="Correo Corporativo" title="Correo Corporativo" /></a>
                    <a id="video-corporativo" href="http://www.youtube.com/v/_mB55ZLT6-A?fs=1&amp;autoplay=1"><img src="<?php echo $aplicacion['static']; ?>/images/video.png" alt="Compromiso Grupo Hinostroza" title="Compromiso Grupo Hinostroza" /></a>
                </div>
                <div id="check-bottom"></div>
            </div>
            <!-- END CHECK IT! -->
            <!-- START TOUR -->
            <!--<div id="tour">
                <div class="tour-icns w-auto only-block m-right"><img src="<?php echo $aplicacion['static']; ?>/images/goodyear_logo.png" alt="goodyear" title="GoodYear" /></div>
                <div class="tour-icns w-auto only-block m-left m-right "><img src="<?php echo $aplicacion['static']; ?>/images/texaco_logo.png" alt="texaco" title="Texaco" /></div>
                <div class="tour-icns w-auto only-block m-left"><img src="<?php echo $aplicacion['static']; ?>/images/jmc_logo.png" alt="" /></div>
                <div class="tour-icns w-auto only-block m-left"><img src="<?php echo $aplicacion['static']; ?>/images/darwin_racing_logo.png" alt="" /></div>
            </div>-->
            <div class="dashed"></div>
            <!-- END TOUR -->
            <!-- START LATEST FROM BLOG -->
            <div class="block">
                <div class="scrollable-wrap" id="chained">
                    <div class="scrollable">
                        <?php
                        $c = 0;
                        foreach ( $db['recursoshumanos']->result() as $itemrecursoshumanos ) {
                            if ( $c%2 == 0 ) {
                                if ( $c != 0 ) echo '</div>';
                                echo '<div class="items">';
                            }
                        ?>
                            <div class="col">
                                <div class="img-holder img-transparent">
                                    <a href="<?php echo $itemrecursoshumanos->link ?>" class="img-holder-overflow"><img src="<?php echo $aplicacion['media']; ?>/<?php echo $itemrecursoshumanos->imagen ?>" alt="<?php echo $itemrecursoshumanos->nombre ?>" /></a>
                                </div>
                                <h3><a href="<?php echo $itemrecursoshumanos->link ?>"><?php echo $itemrecursoshumanos->nombre ?></a></h3>
                                <p><?php echo $itemrecursoshumanos->descripcion ?></p>
                            </div>
                        <?php
                            $c++;
                        }
                        if ( $c%2 != 0 ) echo '</div>';
                        ?>
                    </div>
                </div>
                <!-- START SHORT INFO -->
                <div class="short-info block-bg-blue">
                    <h2><img src="<?php echo $aplicacion['static']; ?>/images/arr-to-left.png" alt="" /><span class="selection">Servicios de mantenimiento técnico</span></h2>
                    <ul class="space list-color minus-line-height">
                        <li><a href="#">Venta y Post Venta</a></li>
                        <li><a href="#">Seguimientos</a></li>
                        <li><a href="#">Cambio de aceite</a></li>
                        <li><a href="#">Lavado y engrase</a></li>
                        <li><a href="#">Mantenimiento y reparación de palieres</a></li>
                        <li><a href="#">Suspención en general</a></li>
                        <li><a href="#">Alineamiento computarizado</a></li>
                        <li><a href="#">Balanceo de llantas estático y dinámico</a></li>
                        <li class="last"><a href="#">Más</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="navi"></div>
            </div>
            <!-- END SHORT INFO -->
            <!-- END LATEST FROM BLOG -->
            <!-- START RECENT PROGECTS -->
            <div class="block">
                <div class="scrollable-wrap1" id="chained1">
                    <div class="scrollable">
                        <?php
                        $c = 0;
                        foreach ( $db['productosgenerales']->result() as $itemproductosgenerales ) {
                            if ( $c%2 == 0 ) {
                                if ( $c != 0 ) echo '</div>';
                                echo '<div class="items">';
                            }
                        ?>
                            <div class="col">
                                <div class="img-holder img-transparent">
                                    <a href="<?php echo $itemproductosgenerales->link ?>" class="img-holder-overflow"><img src="<?php echo $aplicacion['media']; ?>/<?php echo $itemproductosgenerales->imagen ?>" alt="<?php echo $itemproductosgenerales->nombre ?>" /></a>
                                </div>
                                <h3><a href="<?php echo $itemproductosgenerales->link ?>"><?php echo $itemproductosgenerales->nombre ?></a></h3>
                                <p><?php echo $itemproductosgenerales->descripcion ?></p>
                            </div>
                        <?php
                            $c++;
                        }
                        if ( $c%2 != 0 ) echo '</div>';
                        ?>
                    </div>
                </div>
                <!-- START SHORT INFO -->
                <div class="short-info">
                    <h2><img src="<?php echo $aplicacion['static']; ?>/images/arr-to-left.png" alt="" /><span class="selection">Calidad en Nuestros Productos</span></h2>
                    <p class="space">GoodYear proporciona las mejores llantes del mundo. Lo cual nos permite vender calidad.</p>
                    <h3>Testimonios</h3>
                    <p class="testimonials">Se reventó una llanta en el tramo de Lomo Largo, necesitaba ayuda inmediata. Una unidad móvil DEC, fue mi solución.</p>
                    <p class="author">Carlos Carhuaz Q., Conductor minero.</p>
                </div>
                <div class="clear"></div>
                <div class="navi"></div>
            </div>
            <!-- END SHORT INFO -->
            <!-- END RECENT PROGECTS -->
            <!-- START LAST TWEET -->

            <!-- END LAST TWEET -->
            <!-- START FOOTER -->

