<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $aplicacion['titulo']; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?php echo $aplicacion['static']; ?>/css/bootstrap.css" rel="stylesheet">
		<link href="<?php echo $aplicacion['static']; ?>/css/bootstrap-responsive.css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style type="text/css">
			body {
			padding-top: 60px;
			padding-bottom: 40px;
			}
			.sidebar-nav {
			padding: 9px 0;
			}
		</style>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a class="brand" href="#"><?php echo $aplicacion['titulo']; ?></a>
					<div class="btn-group pull-right">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="icon-user"></i> Usuario
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#">Perfil</a></li>
							<li class="divider"></li>
							<li><a href="#">Salir</a></li>
						</ul>
					</div>
					<div class="nav-collapse">
						<ul class="nav">
							<li class="active"><a href="#">Inicio</a></li>
							<li><a href="<?php echo $aplicacion['root_site'] ?>?alumno/">Alumnos</a></li>
							<li><a href="#about">Docentes</a></li>
							<li><a href="#contact">Cursos</a></li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
