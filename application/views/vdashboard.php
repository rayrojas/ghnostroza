    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="nav-header">Alumnos</li>
              <li class="active"><a href="#">Acciones Masivas</a></li>
              <li><a href="#">Buscar Alumnos</a></li>
              <li><a href="#">Matricular Alumno</a></li>
              <li><a href="#">Expulsar Alumno</a></li>
              <li class="nav-header">Profesores</li>
              <li><a href="#">Acciones Masivas</a></li>
              <li><a href="#">Buscar Profesores</a></li>
              <li><a href="#">Ingresar Profesor</a></li>
              <li><a href="#">Habilitar/Desahilitar</a></li>
              <li><a href="#">Asignar Curso</a></li>
              <li class="nav-header">Cursos</li>
              <li><a href="#">Lista de Cursos</a></li>
              <li><a href="#">Crear curso</a></li>
              <li><a href="#">Buscar Curso</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
          <div class="hero-unit">
            <h1>Hello, world!</h1>
            <p>This is a template for a simple marketing or 
informational website. It includes a large callout called the hero unit 
and three supporting pieces of content. Use it as a starting point to 
create something more unique.</p>
            <p><a class="btn btn-primary btn-large">Learn more »</a></p>
          </div>
          <div class="row-fluid">
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
          </div><!--/row-->
          <div class="row-fluid">
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
            <div class="span4">
              <h2>Heading</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce
 dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
              <p><a class="btn" href="#">View details »</a></p>
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>© Company 2012</p>
      </footer>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="fluid_files/jquery.js"></script>
    <script src="fluid_files/bootstrap-transition.js"></script>
    <script src="fluid_files/bootstrap-alert.js"></script>
    <script src="fluid_files/bootstrap-modal.js"></script>
    <script src="fluid_files/bootstrap-dropdown.js"></script>
    <script src="fluid_files/bootstrap-scrollspy.js"></script>
    <script src="fluid_files/bootstrap-tab.js"></script>
    <script src="fluid_files/bootstrap-tooltip.js"></script>
    <script src="fluid_files/bootstrap-popover.js"></script>
    <script src="fluid_files/bootstrap-button.js"></script>
    <script src="fluid_files/bootstrap-collapse.js"></script>
    <script src="fluid_files/bootstrap-carousel.js"></script>
    <script src="fluid_files/bootstrap-typeahead.js"></script>

  

</body></html>
