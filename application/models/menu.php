<?php
	class Menu extends CI_Model {
		var $nombre   = '';
		var $link   = '';
		var $posicion = '';

		function __construct() {
			parent::__construct();
			$this->load->database();
		}

		function model() {
			return array(
				array(
					'field' => 'nombre',
					'verbose_name' => 'Nombre del Servicio',
					'label' => 'Nombre del Servicio',
					'max_length' => 250,
					'help_text' => 'Ingrese el nombre del Servicio.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->nombre
				),
				array(
					'field' => 'link',
					'verbose_name' => 'Enlace del Contenido',
					'label' => 'Enlace del Contenido',
					'max_length' => 250,
					'help_text' => 'Ingrese el enlace del Contenido.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->link
				),
				array(
					'field' => 'posicion',
					'verbose_name' => 'Posicion',
					'label' => 'Posicion',
					'max_length' => 250,
					'help_text' => 'Ingrese la Posicion.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->posicion
				)				
			);
		}
			
		function get($id) {
			$query = $this->db->query("select * from menu WHERE id = ".$id);
			if ($query->num_rows() > 0) {
				$this->nombre = $query->row(0)->nombre;
				$this->link = $query->row(0)->link;
				$this->posicion = $query->row(0)->posicion;
			}
			return $this;
		}
		
		function last($options = '') {
			if ($options == 'array') {
				$query = $this->db->get('menu', 20);
				return $query->result_array();
			} else {
				$query = $this->db->get('menu', 20);
				return $query->result();				
			}
		}

		function data($object) {
			if ( is_array($object) ) {
				$this->nombre = $object['nombre'];
				$this->link = $object['link'];
				$this->posicion = $object['posicion'];
			}
			return $this;
		}

		function save() {
			$this->db->insert('menu', $this);
			return TRUE;
		}
		
		function update($codigo) {
			$this->db->update('menu', $this, array('id' => $codigo));
			return TRUE;
		}

		function update_entry() {
			$this->title   = $_POST['title'];
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->update('entries', $this, array('id' => $_POST['id']));
		}
	}

?>
