<?php
	class Servicio extends CI_Model {
		var $title   = '';
		var $content = '';
		var $date    = '';

		function __construct() {
			// Call the Model constructor
			parent::__construct();
		}
		function model() {
			return array(
				'nombre' => array(
					'field' => 'nombre',
					'verbose_name' => 'Nombre del servicio',
					'help_text' => 'Ingrese el nombre del servicio.',
					'type' => 'input-text',
					'optional' => true
				),
				'descripcion' => array(
					'field' => 'descripcion',
					'verbose_name' => 'Descripción del servicio',
					'help_text' => 'Ingrese la descripción del servicio.',
					'type' => 'input-textarea',
					'optional' => true
				)
			);
		}
		
		function get_last_ten_entries() {
			$query = $this->db->get('entries', 10);
			return $query->result();
		}
		
		function insert_entry() {
			$this->title   = $_POST['title']; // please read the below note
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->insert('entries', $this);
		}

		function update_entry() {
			$this->title   = $_POST['title'];
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->update('entries', $this, array('id' => $_POST['id']));
		}
	}
?>
