<?php
	class Site extends CI_Model {
		var $title   = '';
		var $content = '';
		var $date    = '';

		function __construct() {
			// Call the Model constructor
			parent::__construct();
		}
		function model() {
			return array(
				'nombre' => array(
					'field' => 'nombre',
					'verbose_name' => 'Nombre del sitio',
					'max_length' => 250,
					'help_text' => 'Ingrese el nombre del sitio.',
					'type' => 'input-text',
					'optional' => true
				),
				'slogan' => array(
					'field' => 'slogan',
					'verbose_name' => 'Slogan del sitio',
					'max_length' => 250,
					'help_text' => 'Ingrese el slogan del sitio.',
					'type' => 'input-text',
					'optional' => true
				),
				'subslogan' => array(
					'field' => 'subslogan',
					'verbose_name' => 'Sub Slogan del sitio',
					'max_length' => 250,
					'help_text' => 'Ingrese el sub slogan del sitio.',
					'type' => 'input-text',
					'optional' => true
				),
				'correo' => array(
					'field' => 'correo',
					'verbose_name' => 'Correo Coporativo',
					'max_length' => 250,
					'help_text' => 'Ingrese el Correo Corporativo.',
					'type' => 'input-text',
					'optional' => true
				),
				'video' => array(
					'field' => 'video',
					'verbose_name' => 'Video Coporativo',
					'max_length' => 250,
					'help_text' => 'Ingrese la dirección del Video Coportativo.',
					'type' => 'input-text',
					'optional' => true
				)
			);
		}

		function get_last_ten_entries() {
			$query = $this->db->get('entries', 10);
			return $query->result();
		}
		
		function insert_entry() {
			$this->title   = $_POST['title']; // please read the below note
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->insert('entries', $this);
		}

		function update_entry() {
			$this->title   = $_POST['title'];
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->update('entries', $this, array('id' => $_POST['id']));
		}
	}
?>
