<?php
	class Recursoshumanos extends CI_Model {
		var $nombre   = '';
		var $link   = '';
		var $descripcion = '';
		var $imagen    = '';
		var $estado    = '';
		var $posicion = '';

		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->load->database();
		}

		function model() {
			return array(
				array(
					'field' => 'nombre',
					'verbose_name' => 'Nombre del Recurso Humano',
					'label' => 'Nombre del Recurso Humano',
					'max_length' => 250,
					'help_text' => 'Ingrese el nombre del Recurso Humano.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->nombre
				),
				array(
					'field' => 'link',
					'verbose_name' => 'Enlace del Contenido',
					'label' => 'Enlace del Contenido',
					'max_length' => 250,
					'help_text' => 'Ingrese el enlace del Contenido.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->link
				),				
				array(
					'field' => 'descripcion',
					'verbose_name' => 'Descripción del Recurso Humano',
					'label' => 'Descripción del Recurso Humano',
					'max_length' => 250,
					'help_text' => 'Ingrese la descripción del Recurso Humano.',
					'type' => 'input-textarea',
					'rules' => 'required',
					'value' => $this->descripcion
				),
				array(
					'field' => 'imagen',
					'verbose_name' => 'Imagen',
					'label' => 'Imagen',
					'max_length' => 250,
					'help_text' => 'Seleccione la Imagen.',
					'type' => 'input-file-image',
					'rules' => 'callback_upload[imagen,recursoshumanos/,diapositiva/thumbs/]',
					'value' => $this->imagen
				),
				array(
					'field' => 'estado',
					'verbose_name' => 'Estado',
					'label' => 'Estado',
					'max_length' => 250,
					'help_text' => 'Seleccione el Estado.',
					'type' => 'input-select',
					'choices' => array(array('value'=>'H','text'=>'Habilitado'),array('value'=>'I','text'=>'Inhabilitado')),
					'rules' => 'required',
					'value' => $this->estado
				),
				array(
					'field' => 'posicion',
					'verbose_name' => 'Posicion',
					'label' => 'Posición',
					'max_length' => 250,
					'help_text' => 'Ingrese la posición.',
					'type' => 'input-text',
					'rules' => 'required',
					'value' => $this->posicion
				)
			);
		}
			
		function get($id) {
			$query = $this->db->query("select * from recursoshumanos WHERE id = ".$id);
			if ($query->num_rows() > 0) {
				$this->nombre = $query->row(0)->nombre;
				$this->link = $query->row(0)->link;
				$this->descripcion = $query->row(0)->descripcion;
				$this->imagen = $query->row(0)->imagen;
				$this->estado = $query->row(0)->estado;
				$this->posicion = $query->row(0)->posicion;
			}
			return $this;
		}
		
		function last($options = '') {
			if ($options == 'array') {
				$query = $this->db->get('recursoshumanos', 20);
				return $query->result_array();
			} else {
				$query = $this->db->get('recursoshumanos', 20);
				return $query->result();				
			}
		}

		function data($object) {
			if ( is_array($object) ) {
				$this->nombre = $object['nombre'];
				$this->descripcion = $object['descripcion'];
				$this->link = $object['link'];
				$this->imagen = $object['imagen'];
				$this->estado = $object['estado'];
				$this->posicion = $object['posicion'];
			}
			return $this;
		}

		function save() {
			$this->db->insert('recursoshumanos', $this);
			return TRUE;
		}
		
		function update($codigo) {
			$this->db->update('recursoshumanos', $this, array('id' => $codigo));
			return TRUE;
		}

		function update_entry() {
			$this->title   = $_POST['title'];
			$this->content = $_POST['content'];
			$this->date    = time();
			$this->db->update('entries', $this, array('id' => $_POST['id']));
		}
	}

?>
