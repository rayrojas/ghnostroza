<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Aplicacion.php');
class Forms {
	var $aplicacion = '';
	function Forms() {
		$this->aplicacion = new Aplicacion();
	}	
	public function models($model) {
		$modelArray = $model->model();
		$as_p = "";
		foreach ( $modelArray as $field ) {
			if ( empty($field['value']) ) {
				$field['value'] = '';
			}
			if ( set_value($field['field']) )
				$field['value'] = set_value($field['field']);
			$string = "";
			if ( $field['type'] == 'input-text' ) {
				$string .= $this->input_text($field);
			} else if ( $field['type'] == 'input-textarea' ) {
				$string .= $this->input_textarea($field);
			} else if ( $field['type'] == 'input-file-image' ) {
				$string .=  $this->input_file_image($field);
			} else if ( $field['type'] == 'input-select' ) {
				$string .=  $this->input_select($field);
			} else if (  $field['type'] == 'input-hidden'  ) {
				$string .=  $this->input_hidden($field);
			}
			$as_p .= $string;
		}
		return $as_p;
	}
	public function input_text($field) {
		if ( empty($field['value']) ) {
			$field['value'] = '';
		}
		$string = '<div class="control-group">'.form_error($field['field'], '<div class="alert alert-error">', '</div>');
		$string .= '<label for="id_'.$field['field'].'" class="control-label">'.$field['verbose_name'].'</label>';
		$string .= '<div class="controls">';				
		$string .= '<input class="span5" type="text" id="id_'.$field['field'].'" name="'.$field['field'].'" value="'.$field['value'].'" />';
		$string .= '<span class="help-block">'.$field['help_text'].'</span>';
		$string .= '</div>';
		$string .= '</div>';
		return $string;
	}
	public function input_textarea($field) {
		if ( empty($field['value']) ) {
			$field['value'] = '';
		}
		$string = '<div class="control-group">'.form_error($field['field'], '<div class="alert alert-error">', '</div>');
		$string .= '<label for="id_'.$field['field'].'" class="control-label">'.$field['verbose_name'].'</label>';
		$string .= '<div class="controls">';				
		$string .= '<textarea class="span5" id="id_'.$field['field'].'" name="'.$field['field'].'">'.$field['value'].'</textarea>';
		$string .= '<span class="help-block">'.$field['help_text'].'</span>';
		$string .= '</div>';
		$string .= '</div>';
		return $string;
	}
	public function input_select($field) {
		$string = '<div class="control-group">'.form_error($field['field'], '<div class="alert alert-error">', '</div>');
		$string .= '<label for="id_'.$field['field'].'" class="control-label">'.$field['verbose_name'].'</label>';
		$string .= '<div class="controls">';				
		$string .= '<select class="span5" id="id_'.$field['field'].'" name="'.$field['field'].'"/>';
		foreach ( $field['choices'] as $choice ) {
			if ( !empty($choice) ) {
				if ( $choice['value'] == $field['value'] ) {
					$string .= '<option value="'.$choice['value'].'" selected="selected">'.$choice['text'].'</option>';
				} else {
					$string .= '<option value="'.$choice['value'].'">'.$choice['text'].'</option>';
				}
			}
		}
		$string .= '</select>';
		$string .= '<span class="help-block">'.$field['help_text'].'</span>';
		$string .= '</div>';
		$string .= '</div>';
		return $string;
	}
	public function input_file_image($field) {
		$aplicacion = $this->aplicacion->get_aplicacion();
		$string = '<div class="control-group">'.form_error($field['field'], '<div class="alert alert-error">', '</div>');
		$string .= '<label for="id_'.$field['field'].'" class="control-label">'.$field['verbose_name'].'</label>';
		$string .= '<div class="controls">';
		if ( !empty($field['value']) ) {
			$string .= '<div><img class="thumb-100" src="'.$aplicacion['media'].'/'.$field['value'].'" /></div>';
		}
		$string .= '<input class="span5" type="file" id="id_'.$field['field'].'" name="'.$field['field'].'"/>';
		$string .= '<span class="help-block">'.$field['help_text'].'</span>';
		$string .= '</div>';
		$string .= '</div>';
		return $string;
	}
	public function input_hidden($field) {
		if ( empty($field['value']) ) {
			$field['value'] = '';
			if ( $field['type-data'] == 'date' )
				if ( $field['auto'] ) {
					$field['value'] = date("Y-m-d");
				}			
		}
		$string = '<input class="span5" type="hidden" id="id_'.$field['field'].'" name="'.$field['field'].'" value="'.$field['value'].'" />';
		return $string;
	}
	
}
